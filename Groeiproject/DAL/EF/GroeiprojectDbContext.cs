﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SC.BL.Domain;

namespace SC.DAL.EF
{
    public class GroeiprojectDbContext : DbContext
    {
        public DbSet<Speler> Spelers { get; set; }
        public DbSet<Club> Clubs { get; set; }
        public DbSet<Competitie> Competities { get; set; }

        public DbSet<ClubCompetitie> ClubCompetitie { get; set; }

        public GroeiprojectDbContext()
        {
            //GroeiprojectInitializer.Initialize(this);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //String path = "Data Source=../EF_bestand.db";
            
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder
                    //.UseSqlite(path)
                    .UseSqlServer("Server=tcp:voetbal01.database.windows.net,1433;" +
                                  "Initial Catalog=db-voetbal01;" +
                                  "Persist Security Info=False;" +
                                  "User ID=VoetbalAdmin;" +
                                  "Password=Jeanluc44;" +
                                  "MultipleActiveResultSets=False;" +
                                  "Encrypt=True;" +
                                  "TrustServerCertificate=False;" +
                                  "Connection Timeout=60;")
                    .UseLoggerFactory(LoggerFactory.Create(
                        builder => builder.AddDebug()
                    ));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ClubCompetitie>().Property<int>("ClubId");
            modelBuilder.Entity<ClubCompetitie>().Property<int>("CompetitieId");
            modelBuilder.Entity<ClubCompetitie>().HasKey("ClubId", "CompetitieId");
            
            modelBuilder.Entity<ClubSpeler>().Property<int>("ClubId");
            modelBuilder.Entity<ClubSpeler>().Property<int>("SpelerId");
            modelBuilder.Entity<ClubSpeler>().HasKey("ClubId", "SpelerId");
            
            Club club01 = new Club(-1, "KV Mechelen", "België", 18477, new DateTime(1904, 10, 1));
            Club club02 = new Club(-2, "Royal Antwerp FC", "België", 16649, new DateTime(1880, 9, 1));
            Club club03 = new Club(-3, "Liverpool", "Verenigd Koninkrijk", 54074, new DateTime(1892, 3, 15));
            Club club04 = new Club(-4, "Borussia Dortmund", "Duitsland", 81365, new DateTime(1909, 12, 19));

            Competitie competitie01 = new Competitie(-1, "Jupiler Pro League", "België", 1895);
            Competitie competitie02 = new Competitie(-2, "Premier League", "Verenigd Koninkrijk", 1992);
            Competitie competitie03 = new Competitie(-3, "Bundesliga", "Duitsland", 1963);
            Competitie competitie04 = new Competitie(-4, "Champions League", "Europa", 1992);

            Speler speler01 = new Speler(-1, "Aster Vranckx", 40, 1.83, new DateTime(2002, 10, 4),
                SpelerPostitie.Middenvelder);
            Speler speler02 = new Speler(-2, "Gaëtan Coucke", 28, 1.87, new DateTime(1998, 11, 3),
                SpelerPostitie.Doelman);
            Speler speler03 = new Speler(-3, "Dieumerci Mbokani", 70, 1.85, new DateTime(1985, 11, 22),
                SpelerPostitie.Aanvaller);
            Speler speler04 = new Speler(-4, "Virgil van Dijk", 4, 1.93, new DateTime(1991, 7, 8),
                SpelerPostitie.Verdediger);
            Speler speler05 = new Speler(-5, "Erling Braut Haaland", 9, 1.94, new DateTime(2000, 7, 21),
                SpelerPostitie.Aanvaller);

            modelBuilder.Entity<Speler>().HasData(speler01, speler02, speler03, speler04, speler05);
            modelBuilder.Entity<Club>().HasData(club01, club02, club03, club04);
            modelBuilder.Entity<Competitie>().HasData(competitie01, competitie02, competitie03, competitie04);

            modelBuilder.Entity<ClubSpeler>().HasData(new {ClubId = -1, SpelerId = -1},
                new {ClubId = -1, SpelerId = -2}, new {ClubId = -2, SpelerId = -3},
                new {ClubId = -3, SpelerId = -4}, new {ClubId = -4, SpelerId = -5});
            
            modelBuilder.Entity<ClubCompetitie>().HasData(new {ClubId = -1, CompetitieId = -1},
                new {ClubId = -2, CompetitieId = -1}, new {ClubId = -3, CompetitieId = -2},
                new {ClubId = -4, CompetitieId = -3}, new {ClubId = -1, CompetitieId = -4});
        }
    }
}