﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using SC.BL.Domain;

namespace SC.DAL.EF
{
    public class Repository : IRepository
    {
        private GroeiprojectDbContext DbContext;

        public Repository()
        {
            DbContext = new GroeiprojectDbContext();
        }


        public Club ReadClub(int id)
        {
            return DbContext.Clubs
                .Single(c => c.Id.Equals(id));
        }

        public Club ReadClubWithCompetitionAndPlayers(int id)
        {
            return DbContext.Clubs
                .Include(c => c.ClubInCompetities)
                .ThenInclude(cc => cc.Competitie)
                .Include(c => c.SpelersInClub)
                .ThenInclude(cs => cs.Speler)
                .Single(c => c.Id.Equals(id));
        }

        public List<Club> ReadAllClubs()
        {
            return DbContext.Clubs.AsEnumerable().ToList();
        }

        public List<Club> ReadAllClubsWithCompetitions()
        {
            return DbContext.Clubs
                .Include(c => c.ClubInCompetities)
                .ThenInclude(cc => cc.Competitie)
                .AsEnumerable().ToList();
        }

        public List<Club> ReadClubsOfCountry(string country)
        {
            return DbContext.Clubs.Where(c => c.Land.Equals(country)).AsEnumerable().ToList();
        }

        public Competitie ReadClubsOfCompetition(int competitieId)
        {
            return DbContext.Competities
                .Where(c => c.Id.Equals(competitieId))
                .Include(c => c.ClubsInCompetities)
                .ThenInclude(cc => cc.Club).Single();
        }

        public void CreateClub(Club club)
        {
            DbContext.Clubs.Add(club);
            DbContext.SaveChanges();
        }


        public Competitie ReadCompetition(int id)
        {
            return DbContext.Competities.Single(c => c.Id.Equals(id));
        }

        public List<Competitie> ReadAllCompetitions()
        {
            return DbContext.Competities.AsEnumerable().ToList();
        }

        public List<Competitie> ReadAllCompetitionsWithClubs()
        {
            return DbContext.Competities
                .Include(c => c.ClubsInCompetities)
                .ThenInclude(cc => cc.Club)
                .AsEnumerable().ToList();
        }

        public List<Competitie> ReadCompetitionsByNameAndOprichtingsjaar(string name, int oprichtingsjaar)
        {
            return DbContext.Competities.Where(c =>
                    c.Naam.ToLower().Contains(name.ToLower()) && c.Oprichtingsjaar < oprichtingsjaar)
                .AsEnumerable().ToList();
        }

        public void CreateCompetition(Competitie competitie)
        {
            DbContext.Competities.Add(competitie);
            DbContext.SaveChanges();
        }


        public void CreateClubCompetition(ClubCompetitie clubCompetitie)
        {
            foreach (ClubCompetitie cc in DbContext.ClubCompetitie.Include(cc => cc.Club)
                .Include(cc => cc.Competitie))
            {
                if (cc.Club.Equals(clubCompetitie.Club) && cc.Competitie.Equals(clubCompetitie.Competitie))
                {
                    return;
                }
            }

            DbContext.ClubCompetitie.Add(clubCompetitie);
            DbContext.SaveChanges();
        }

        public void DeleteClubCompetition(int clubId, int competitieId)
        {
            foreach (Competitie competitie in DbContext.Competities.Include(c => c.ClubsInCompetities)
                .ThenInclude(cc => cc.Club))
            {
                if (competitie.Id.Equals(competitieId))
                {
                    foreach (ClubCompetitie cc in competitie.ClubsInCompetities)
                    {
                        if (cc.Club.Id.Equals(clubId))
                        {
                            DbContext.ClubCompetitie.Remove(cc);
                        }
                    }
                }
            }

            DbContext.SaveChanges();
        }

        public Speler ReadPlayer(int id)
        {
            return DbContext.Spelers.Single(s => s.Id.Equals(id));
        }

        public List<Speler> ReadAllPlayers()
        {
            return DbContext.Spelers.AsEnumerable().ToList();
        }


        public List<Speler> ReadAllPlayersWithClub()
        {
            return DbContext.Spelers
                .Include(s => s.ClubVanSpeler)
                .ThenInclude(cs => cs.Club)
                .AsEnumerable().ToList();
        }
    }
}