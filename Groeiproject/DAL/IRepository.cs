﻿using System.Collections.Generic;
using SC.BL.Domain;

namespace SC.DAL
{
    public interface IRepository
    {
        public Club ReadClub(int id);
        public Club ReadClubWithCompetitionAndPlayers(int id);
        public List<Club> ReadAllClubs();
        public List<Club> ReadAllClubsWithCompetitions();
        public List<Club> ReadClubsOfCountry(string country);
        public Competitie ReadClubsOfCompetition(int competitieId);
        public void CreateClub(Club club);

        public Competitie ReadCompetition(int id);
        public List<Competitie> ReadAllCompetitions();
        public List<Competitie> ReadAllCompetitionsWithClubs();
        public List<Competitie> ReadCompetitionsByNameAndOprichtingsjaar(string name, int oprichtingsjaar);
        public void CreateCompetition(Competitie competitie);

        public void CreateClubCompetition(ClubCompetitie clubCompetitie);
        public void DeleteClubCompetition(int clubId, int competitieId);

        public Speler ReadPlayer(int id);
        public List<Speler> ReadAllPlayers();
        public List<Speler> ReadAllPlayersWithClub();
    }
}