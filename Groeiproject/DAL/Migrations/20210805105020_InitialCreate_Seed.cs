﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SC.DAL.Migrations
{
    public partial class InitialCreate_Seed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Clubs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Naam = table.Column<string>(maxLength: 25, nullable: false),
                    Land = table.Column<string>(nullable: false),
                    CapaciteitStadion = table.Column<int>(nullable: false),
                    Oprichtingsdatum = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Clubs", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Competities",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Naam = table.Column<string>(maxLength: 25, nullable: false),
                    Locatie = table.Column<string>(maxLength: 25, nullable: true),
                    Oprichtingsjaar = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Competities", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Spelers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Naam = table.Column<string>(nullable: true),
                    Rugnummer = table.Column<int>(nullable: false),
                    Grootte = table.Column<double>(nullable: true),
                    Geboortedatum = table.Column<DateTime>(nullable: false),
                    Positie = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Spelers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ClubCompetitie",
                columns: table => new
                {
                    ClubId = table.Column<int>(nullable: false),
                    CompetitieId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClubCompetitie", x => new { x.ClubId, x.CompetitieId });
                    table.ForeignKey(
                        name: "FK_ClubCompetitie_Clubs_ClubId",
                        column: x => x.ClubId,
                        principalTable: "Clubs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ClubCompetitie_Competities_CompetitieId",
                        column: x => x.CompetitieId,
                        principalTable: "Competities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ClubSpeler",
                columns: table => new
                {
                    ClubId = table.Column<int>(nullable: false),
                    SpelerId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClubSpeler", x => new { x.ClubId, x.SpelerId });
                    table.ForeignKey(
                        name: "FK_ClubSpeler_Clubs_ClubId",
                        column: x => x.ClubId,
                        principalTable: "Clubs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ClubSpeler_Spelers_SpelerId",
                        column: x => x.SpelerId,
                        principalTable: "Spelers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Clubs",
                columns: new[] { "Id", "CapaciteitStadion", "Land", "Naam", "Oprichtingsdatum" },
                values: new object[,]
                {
                    { -1, 18477, "België", "KV Mechelen", new DateTime(1904, 10, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { -2, 16649, "België", "Royal Antwerp FC", new DateTime(1880, 9, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { -3, 54074, "Verenigd Koninkrijk", "Liverpool", new DateTime(1892, 3, 15, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { -4, 81365, "Duitsland", "Borussia Dortmund", new DateTime(1909, 12, 19, 0, 0, 0, 0, DateTimeKind.Unspecified) }
                });

            migrationBuilder.InsertData(
                table: "Competities",
                columns: new[] { "Id", "Locatie", "Naam", "Oprichtingsjaar" },
                values: new object[,]
                {
                    { -1, "België", "Jupiler Pro League", 1895 },
                    { -2, "Verenigd Koninkrijk", "Premier League", 1992 },
                    { -3, "Duitsland", "Bundesliga", 1963 },
                    { -4, "Europa", "Champions League", 1992 }
                });

            migrationBuilder.InsertData(
                table: "Spelers",
                columns: new[] { "Id", "Geboortedatum", "Grootte", "Naam", "Positie", "Rugnummer" },
                values: new object[,]
                {
                    { -1, new DateTime(2002, 10, 4, 0, 0, 0, 0, DateTimeKind.Unspecified), 1.8300000000000001, "Aster Vranckx", 1, 40 },
                    { -2, new DateTime(1998, 11, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), 1.8700000000000001, "Gaëtan Coucke", 3, 28 },
                    { -3, new DateTime(1985, 11, 22, 0, 0, 0, 0, DateTimeKind.Unspecified), 1.8500000000000001, "Dieumerci Mbokani", 0, 70 },
                    { -4, new DateTime(1991, 7, 8, 0, 0, 0, 0, DateTimeKind.Unspecified), 1.9299999999999999, "Virgil van Dijk", 2, 4 },
                    { -5, new DateTime(2000, 7, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), 1.9399999999999999, "Erling Braut Haaland", 0, 9 }
                });

            migrationBuilder.InsertData(
                table: "ClubCompetitie",
                columns: new[] { "ClubId", "CompetitieId" },
                values: new object[,]
                {
                    { -1, -1 },
                    { -2, -1 },
                    { -3, -2 },
                    { -4, -3 },
                    { -1, -4 }
                });

            migrationBuilder.InsertData(
                table: "ClubSpeler",
                columns: new[] { "ClubId", "SpelerId" },
                values: new object[,]
                {
                    { -1, -1 },
                    { -1, -2 },
                    { -2, -3 },
                    { -3, -4 },
                    { -4, -5 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_ClubCompetitie_CompetitieId",
                table: "ClubCompetitie",
                column: "CompetitieId");

            migrationBuilder.CreateIndex(
                name: "IX_ClubSpeler_SpelerId",
                table: "ClubSpeler",
                column: "SpelerId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ClubCompetitie");

            migrationBuilder.DropTable(
                name: "ClubSpeler");

            migrationBuilder.DropTable(
                name: "Competities");

            migrationBuilder.DropTable(
                name: "Clubs");

            migrationBuilder.DropTable(
                name: "Spelers");
        }
    }
}
