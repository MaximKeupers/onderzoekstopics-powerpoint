﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SC.BL.Domain
{
    public class Club : IValidatableObject
    {
        [Key]
        public int Id { get; set; }
        [Required(ErrorMessage = "The name field is required.")]
        [StringLength(25)]
        public string Naam { get; set; }
        [Required(ErrorMessage = "The country field is required.")]
        public string Land { get; set; }
        [Required(ErrorMessage = "The capacity field is required.")]
        [Range(0,99999, ErrorMessage = "The capacity must be between 0 and 99999") ]
        public int CapaciteitStadion { get; set; }
        [Required(ErrorMessage = "The date field has to be after 1850 and before today.")]
        public DateTime Oprichtingsdatum { get; set; }
        public ICollection<ClubCompetitie> ClubInCompetities { get; set; }
        public ICollection<ClubSpeler> SpelersInClub { get; set; }
        public String Trainer { get; set; }

        public Club()
        {
            this.Id = 0;
            this.Naam = "Onbekend";
            this.Land = "Onbekend";
            this.CapaciteitStadion = 0;
            this.Oprichtingsdatum = new DateTime(1900, 1, 1);
            this.Trainer = "Onbekend";
        }
        
        public Club(string naam, string land, int capaciteitStadion, DateTime oprichtingsdatum, String trainer)
        {
            this.Naam = naam;
            this.Land = land;
            this.CapaciteitStadion = capaciteitStadion;
            this.Oprichtingsdatum = oprichtingsdatum;
            this.Trainer = trainer;
        }
        
        public Club(string naam, string land, int capaciteitStadion, DateTime oprichtingsdatum)
        {
            this.Naam = naam;
            this.Land = land;
            this.CapaciteitStadion = capaciteitStadion;
            this.Oprichtingsdatum = oprichtingsdatum;
        }
        
        public Club(int id, string naam, string land, int capaciteitStadion, DateTime oprichtingsdatum)
        {
            this.Id = id;
            this.Naam = naam;
            this.Land = land;
            this.CapaciteitStadion = capaciteitStadion;
            this.Oprichtingsdatum = oprichtingsdatum;
        }
        
        public Club(int id, string naam, string land, int capaciteitStadion, DateTime oprichtingsdatum, String trainer)
        {
            this.Id = id;
            this.Naam = naam;
            this.Land = land;
            this.CapaciteitStadion = capaciteitStadion;
            this.Oprichtingsdatum = oprichtingsdatum;
            this.Trainer = trainer;
        }

        public override string ToString()
        {
            return $"{Naam, -27}{Land, -21}{CapaciteitStadion, -8}{Oprichtingsdatum.ToLongDateString(), -29}{Trainer, -20}";
        }
        
        IEnumerable<ValidationResult> IValidatableObject.Validate(ValidationContext context)
        {
            List<ValidationResult> errors = new List<ValidationResult>();

            if (this.Oprichtingsdatum > DateTime.Now || this.Oprichtingsdatum < new DateTime(1850, 1, 1))
            {
                errors.Add(new ValidationResult("The date field has to be after 1850 and before today."));
            }

            return errors;
        }
    }
}