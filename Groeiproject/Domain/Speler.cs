﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace SC.BL.Domain
{
    public class Speler
    {
        [Key] public int Id { get; set; }
        public string Naam { get; set; }
        public int Rugnummer { get; set; }
        public double? Grootte { get; set; }
        public DateTime Geboortedatum { get; set; }
        public SpelerPostitie Positie { get; set; }

        public ICollection<ClubSpeler> ClubVanSpeler { get; set; }

        public Speler()
        {
            this.Id = 0;
            this.Naam = "Onbekend";
            this.Rugnummer = 99;
            this.Grootte = 0.0;
            this.Geboortedatum = new DateTime(1900, 1, 1);
            this.Positie = SpelerPostitie.Onbekend;
        }

        public Speler(int id, string naam, int rugnummer, double grootte, DateTime geboortedatum,
            SpelerPostitie positie)
        {
            this.Id = id;
            this.Naam = naam;
            this.Rugnummer = rugnummer;
            this.Grootte = grootte;
            this.Geboortedatum = geboortedatum;
            this.Positie = positie;
        }

        public override string ToString()
        {
            return
                $"{Naam,-22}{Rugnummer,-4}{Grootte,-6}{Geboortedatum.ToLongDateString(),-29}{Positie,-14}{ClubVanSpeler.Last().Club.Naam,-27}";
        }
    }
}