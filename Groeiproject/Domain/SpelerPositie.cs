﻿namespace SC.BL.Domain
{
    public enum SpelerPostitie
    {
        Aanvaller = 0,
        Middenvelder,
        Verdediger,
        Doelman,
        Onbekend
    }
}