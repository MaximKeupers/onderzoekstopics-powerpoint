﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using SC.BL;
using SC.BL.Domain;

namespace SC.UI.CA
{
    static class Program
    {
        private static Manager _manager;

        private static void Main()
        {
            _manager = new Manager();

            while (true)
            {
                switch (ShowMenu())
                {
                    case 0:
                        Environment.Exit(0);
                        break;
                    case 1:
                        AlleClubs();
                        break;
                    case 2:
                        AlleClubsVanLand();
                        break;
                    case 3:
                        AlleCompetities();
                        break;
                    case 4:
                        AlleCompetitiesMetNaamEnJaar();
                        break;
                    case 5:
                        VoegClubToe();
                        break;
                    case 6:
                        VoegCompetitieToe();
                        break;
                    case 7:
                        VoegClubAanCompetitieToe();
                        break;
                    case 8:
                        VerwijderClubVanCompetitie();
                        break;
                    case 9:
                        AlleSpelers();
                        break;
                }

                Console.WriteLine("\n");
            }

            static int ShowMenu()
            {
                string keuze = "";

                while (!(keuze == "0" || keuze == "1" || keuze == "2" || keuze == "3" || keuze == "4" || keuze == "5" ||
                         keuze == "6" || keuze == "7" || keuze == "8" || keuze == "9"))
                {
                    Console.WriteLine("Wat wilt u doen?");
                    Console.WriteLine("==========================");
                    Console.WriteLine("0) Stoppen");
                    Console.WriteLine("1) Toon alle clubs");
                    Console.WriteLine("2) Toon alle clubs van land");
                    Console.WriteLine("3) Toon alle competities");
                    Console.WriteLine("4) Toon alle competities met naam en oprichtingsjaar");
                    Console.WriteLine("5) Voeg een club toe");
                    Console.WriteLine("6) Voeg een competitie toe");

                    Console.WriteLine("7) Voeg een club aan een competitie toe");
                    Console.WriteLine("8) Verwijder een club van een competitie");

                    Console.WriteLine("9) Toon alle spelers");

                    Console.Write("Choice (0-9): ");
                    keuze = Console.ReadLine();
                    Console.WriteLine();
                }

                return Convert.ToInt16(keuze);
            }


            static void AlleClubs()
            {
                Console.WriteLine("Alle clubs:");
                foreach (Club club in _manager.GetAllClubsWithCompetitions())
                {
                    Console.WriteLine(club.Naam + " speelt in:");
                    foreach (ClubCompetitie clubCompetitie in club.ClubInCompetities)
                    {
                        Console.WriteLine("\t- " + clubCompetitie.Competitie.Naam);
                    }

                    Console.WriteLine();
                }
            }

            static void AlleClubsVanLand()
            {
                string keuze = "";
                while (!(keuze == "1" || keuze == "2" || keuze == "3"))
                {
                    Console.Write("Land (1=België, 2=Verenigd Koninkrijk, 3=Duitsland): ");
                    keuze = Console.ReadLine();
                }

                switch (Convert.ToInt16(keuze))
                {
                    case 1:
                        foreach (Club club in _manager.GetClubsOfCountry("België"))
                        {
                            Console.WriteLine(
                                $"{club.Naam,-27}{club.CapaciteitStadion,-8}{club.Oprichtingsdatum.ToLongDateString(),-29}{club.Trainer,-20}");
                        }

                        break;
                    case 2:
                        foreach (Club club in _manager.GetClubsOfCountry("Verenigd Koninkrijk"))
                        {
                            Console.WriteLine(
                                $"{club.Naam,-27}{club.CapaciteitStadion,-8}{club.Oprichtingsdatum.ToLongDateString(),-29}{club.Trainer,-20}");
                        }

                        break;
                    case 3:
                        foreach (Club club in _manager.GetClubsOfCountry("Duitsland"))
                        {
                            Console.WriteLine(
                                $"{club.Naam,-27}{club.CapaciteitStadion,-8}{club.Oprichtingsdatum.ToLongDateString(),-29}{club.Trainer,-20}");
                        }

                        break;
                }
            }

            static void AlleCompetities()
            {
                Console.WriteLine("Alle competities:");
                foreach (Competitie competitie in _manager.GetAllCompetitionsWithClubs())
                {
                    Console.WriteLine(competitie.Naam);
                    foreach (ClubCompetitie clubCompetitie in competitie.ClubsInCompetities)
                    {
                        Console.WriteLine("\t- " + clubCompetitie.Club.Naam);
                    }

                    Console.WriteLine();
                }
            }

            static void AlleCompetitiesMetNaamEnJaar()
            {
                Console.Write("Geef een (stuk van een) naam of niks in: ");
                string naam = Console.ReadLine();

                string oprichtingsjaar = "";

                while (!IsNumber(oprichtingsjaar))
                {
                    Console.Write("Geef een oprichtingsjaar waarvoor de competitie is opgericht of niks in: ");
                    oprichtingsjaar = Console.ReadLine();
                }

                foreach (Competitie competitie in _manager
                    .GetCompetitionsByNameAndOprichtingsjaar(naam, Convert.ToInt32(oprichtingsjaar)))
                {
                    Console.WriteLine(competitie.ToString());
                }
            }

            static void VoegClubToe()
            {
                Console.WriteLine("Voeg club toe");
                Console.WriteLine("=============");

                bool valid = false;

                while (!valid)
                {
                    Console.Write("Naam: ");
                    string naamInput = Console.ReadLine();

                    Console.Write("Land: ");
                    string landInput = Console.ReadLine();

                    Console.Write("Capaciteit van het stadion: ");
                    string capaciteitStadionInputString = Console.ReadLine();

                    if (!int.TryParse(capaciteitStadionInputString, out int capaciteitStadionInput))
                    {
                        capaciteitStadionInput = -1;
                    }

                    string oprichtingsdatumInputString = "";
                    while (!IsDateTime(oprichtingsdatumInputString))
                    {
                        Console.Write("Oprichtingsdatum: ");
                        oprichtingsdatumInputString = Console.ReadLine();
                    }

                    DateTime oprichtingsdatumInput = Convert.ToDateTime(oprichtingsdatumInputString);
                    
                    Console.Write("Trainer: ");
                    string trainerInput = Console.ReadLine();

                    Club newClub = _manager.AddClub(naamInput, landInput, capaciteitStadionInput,
                        oprichtingsdatumInput, trainerInput);

                    List<ValidationResult> errors = new List<ValidationResult>();
                    valid = Validator.TryValidateObject(newClub, new ValidationContext(newClub), errors,
                        true);

                    if (!valid)
                    {
                        foreach (ValidationResult error in errors)
                        {
                            Console.WriteLine(error.ErrorMessage);
                        }
                    }

                    Console.WriteLine();
                }
            }

            static void VoegCompetitieToe()
            {
                Console.WriteLine("Voeg competitie toe");
                Console.WriteLine("=============");

                Console.Write("Naam: ");
                string naamComp = Console.ReadLine();

                Console.Write("Locatie: ");
                string locatieComp = Console.ReadLine();

                string oprichtingsjaarCompString = "";
                while (!IsNumber(oprichtingsjaarCompString))
                {
                    Console.Write("Oprichtingsjaar: ");
                    oprichtingsjaarCompString = Console.ReadLine();
                }

                int oprichtingsjaarComp = Convert.ToInt32(oprichtingsjaarCompString);

                Competitie newCompetitie = _manager.AddCompetition(naamComp, locatieComp, oprichtingsjaarComp);

                List<ValidationResult> errorsComp = new List<ValidationResult>();
                bool valid = Validator.TryValidateObject(newCompetitie, new ValidationContext(newCompetitie),
                    errorsComp,
                    true);

                if (!valid)
                {
                    foreach (ValidationResult error in errorsComp)
                    {
                        Console.WriteLine(error.ErrorMessage);
                    }
                }

                Console.WriteLine();
            }

            static void VoegClubAanCompetitieToe()
            {
                Club gekozenClub = null;
                Competitie gekozenCompetitie = null;

                List<Competitie> alleCompetities = _manager.GetAllCompetitions();
                List<Club> alleClubs = _manager.GetAllClubs();

                bool competitieOk = true;
                do
                {
                    Console.WriteLine("Aan welke competitie wil je een club toevoegen?");
                    int teller = 1;
                    foreach (Competitie competitie in alleCompetities)
                    {
                        Console.WriteLine("[" + teller + "] " + competitie.Naam);
                        teller++;
                    }

                    Console.Write("Typ een competitie ID: ");
                    string keuze = Console.ReadLine();
                    Console.WriteLine();

                    int i = 1;
                    while (i != alleCompetities.Count + 1)
                    {
                        if (keuze == i.ToString())
                        {
                            competitieOk = false;
                            gekozenCompetitie = alleCompetities[i - 1];
                        }

                        i++;
                    }
                } while (competitieOk);

                bool clubOk = true;
                do
                {
                    Console.WriteLine("Welke club wil je aan deze competitie toevoegen?");
                    int teller = 1;
                    foreach (Club club in alleClubs)
                    {
                        Console.WriteLine("[" + teller + "] " + club.Naam);
                        teller++;
                    }

                    Console.Write("Typ een club ID: ");
                    string keuze = Console.ReadLine();
                    Console.WriteLine();

                    int i = 1;
                    while (i != alleClubs.Count + 1)
                    {
                        if (keuze == i.ToString())
                        {
                            clubOk = false;
                            gekozenClub = alleClubs[i - 1];
                        }

                        i++;
                    }
                } while (clubOk);

                _manager.AddClubCompetition(new ClubCompetitie {Club = gekozenClub, Competitie = gekozenCompetitie});
            }

            static void VerwijderClubVanCompetitie()
            {
                int competitieId = 1;
                int clubId = 1;

                List<Competitie> alleCompetities = _manager.GetAllCompetitions();

                bool competitieOk = true;
                do
                {
                    Console.WriteLine("Uit welke competitie wil je een club verwijderen?");
                    int teller = 1;
                    foreach (Competitie competitie in alleCompetities)
                    {
                        Console.WriteLine("[" + teller + "] " + competitie.Naam);
                        teller++;
                    }

                    Console.Write("Typ een competitie ID: ");
                    string keuze = Console.ReadLine();
                    Console.WriteLine();

                    int i = 1;
                    while (i != alleCompetities.Count + 1)
                    {
                        if (keuze == i.ToString())
                        {
                            competitieOk = false;
                            competitieId = i;
                        }

                        i++;
                    }
                } while (competitieOk);

                Competitie alleClubsVanCompetitie = _manager.GetClubsOfCompetition(competitieId);

                bool clubOk = true;
                do
                {
                    Console.WriteLine("Welke club wil je uit deze competitie verwijderen?");
                    int teller = 1;
                    foreach (ClubCompetitie cc in alleClubsVanCompetitie.ClubsInCompetities)
                    {
                        Console.WriteLine("[" + teller + "] " + cc.Club.Naam);
                        teller++;
                    }

                    Console.Write("Typ een club ID: ");
                    string keuze = Console.ReadLine();
                    Console.WriteLine();

                    int i = 1;
                    while (i != alleClubsVanCompetitie.ClubsInCompetities.Count + 1)
                    {
                        if (keuze == i.ToString())
                        {
                            clubOk = false;
                            clubId = alleClubsVanCompetitie.ClubsInCompetities.ToArray()[i - 1].Club.Id;
                        }

                        i++;
                    }
                } while (clubOk);

                _manager.RemoveClubCompetition(clubId, competitieId);
            }

            static void AlleSpelers()
            {
                Console.WriteLine("Alle spelers:");
                foreach (Speler speler in _manager.GetAllPlayersWithClub())
                {
                    Console.WriteLine(speler.ToString());
                }
            }


            static bool IsNumber(string txt)
            {
                return int.TryParse(txt, out _);
            }

            static bool IsDateTime(string txt)
            {
                return DateTime.TryParse(txt, out _);
            }
        }
    }
}